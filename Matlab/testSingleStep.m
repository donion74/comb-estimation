function [QFI_dual, QFI_primal] = testSingleStep(N, i, gate)
    %% calculates the QFI for one time step i, can be used for testing.
    %
    % input
    % ---------------------------------------------------------------------
    % i:        iteration corresponding to time step i.
    % N:        defines the order of the quantum N-comb.
    % gate:     generator of the interaction, available options are 'swap',
    %           'cnot_environment', 'cnot_system', 'bitflip'
    
    % output
    % ---------------------------------------------------------------------
    % QFI_dual: QFI of the dual problem
    
    arguments
        N = 1;
        i = 44;
        gate = "bitflip"; % "cnot_environment", "cnot_system, "swap, bitflip"
    end

    dpur = 1;
    syms g tau theta t real;
    variables = [tau, g, t, theta];
    iter = 196;
    gi = ones(iter,1);
    thetai = 2*pi/20 * ones(iter,1);
    time = linspace(0,21,iter);
    QFI_expected = (time(i))^2
    time = time/N;
    taui = time;
    copies = 1;
    
    createU = str2func(gate);
    [U, U_controlfree, U_Markov, U_Markov_controlfree] ...
        = createUnitaries(tau, g, t, theta, gate, copies);
    verbose = 2;
    primal = 1;
    collisionOnly = 0;
    daux = 1;

    markov = 1;
    if markov
        variables = [tau, g, t, theta];
        var_values = [taui(i), gi(i), time(i), thetai(i)];
        QFI_Markov = markovControl(U_Markov, N, ...
                                                variables, var_values, dpur);
        QFI_Markov
        QFI_dual = 0;
        QFI_primal = 0;
    else
        [QFI_dual, QFI_no_control, QFI_Markov, QFI_Markov_controlfree, ...
        QFI_Heisenberg, QFI_primal, h, T] = ...
             simulateTimeStep(i, N, dpur, variables, taui, gi, time, ...
             thetai, U, U_controlfree, U_Markov, U_Markov_controlfree, ...
             verbose, primal, collisionOnly, gate, daux, copies);
        disp("Heisenberg, dual, primal, non-Markov no control, Markov control, no control")
        [QFI_Heisenberg, QFI_dual, QFI_primal QFI_no_control, QFI_Markov, ...
                QFI_Markov_controlfree]/copies^2
    end
   
    if verbose >= 3
        if daux == 1
            T
            0.5 * diag(kron(T, eye(2)))'
        elseif daux == 2
            PartialTrace(T, 2*N, [2*ones(1,2*N-1) daux])
            diag(T)'
        end
    end
    
        
end