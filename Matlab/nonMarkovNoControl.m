function QFI = nonMarkovNoControl(U, N, var, var_values, initial_state, copies, dpur)
    %% calculates the QFI for the non-Markovian case without control.
    %
    % input
    % ---------------------------------------------------------------------
    % U:        unitary of the interaction
    % N:        order of quantum N-comb
    % var:      variables, default [tau, g, t, theta]
    % var_values:  numerical values of var.
    % dpur:     dimension of purifying system.
    %
    % output
    % ---------------------------------------------------------------------
    % QFI:      QFI of the scenario.
    
    Utot = U^N;
    shape = size(Utot);
    dimS = sqrt(shape(1)); % assume that system and environment have same dim.
    dimE = dimS;
    dimensions = [dimS,dimS];
    totalDim = dimE*dimS;
    
    if initial_state == "zero"
        Ctheta = sym(zeros(totalDim,dimE));
        Ctheta(:,1) = reshape(transpose(Utot(1:2,1:2)),[],1);
        Ctheta(:,2) = reshape(transpose(Utot(3:4,1:2)),[],1);
        assert( all(size(Ctheta) == [dimS^2, dimE]));
    elseif initial_state == "maximal_mixed"    
        Ctheta = sym(zeros(totalDim, dimE*dimE));
        for i=1:dimE
            for j=1:dimE
                Ctheta(:,(j-1)*dimE + i) = reshape(transpose(Utot...
                    (dimE*(i-1)+1:dimE*(i-1)+dimS,dimE*(j-1)+1:dimE*(j-1)+dimS)),[],1);
            end
        end
        Ctheta = Ctheta / sqrt(dimE);
        assert( all(size(Ctheta) == [dimS^2, dimE^2]));
    end
       
        
    for i=1:length(var)-1
        Ctheta = subs(Ctheta,var(i),var_values(i));
    end
    
    % Ctheta now has indices (k_2,k_1), but we want (k_1, k_2)
    sizeCtheta = size(Ctheta);
    C_swap = sym(zeros(sizeCtheta(1),sizeCtheta(2)));
    bits = log2(totalDim);
    for i=1:totalDim
        i_new = swapBinary(i,bits);
        C_swap(i_new,:) = Ctheta(i,:);
    end
        
    theta_value = var_values(end);
    QFI = minimalQFI(C_swap, theta_value, dimensions, dpur);
end