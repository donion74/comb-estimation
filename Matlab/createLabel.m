function str = createLabel(t)
	% creates the label for generated data,
	% of the form yyyy-mm-dd_hh-mi-ss-mls,
	% where yyyy is the year, mm the month,
	% dd the day, hh the hour, mi the minute,
	% ss the second, mls the milliseconds,
	% e.g., 2021-03-09_09-24-13-324
    arguments
        t = datetime
    end
    current_time = t;
    str = year(current_time) + "-" + num2str(month(current_time),'%02.f') + "-" + num2str(day(current_time),'%02.f') + "_";
    str = str + num2str(hour(current_time), '%02.f') + "-" + num2str(minute(current_time),'%02.f') + "-";
    ms = ceil(1000 * (second(current_time) - floor(second(current_time))));
    str = str +  num2str(floor(second(current_time)),'%02.f') + "-" + num2str(ms,'%03.f');
end
