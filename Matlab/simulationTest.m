% test for simulation for N=2 and interactions swap, cnot environment, cnot
% system, bitflip


%% SWAP N=2
gate = "swap";
csv = "swap-identity-N-2_2021-02-24_13-48-26-537.csv";
test(gate, csv);

%% CNOT environment
gate = "cnot_environment";
csv = "cnot_environment-identity-N-2_2021-03-09_09-09-36-510.csv";
csv = "cnot_environment-identity-N-2_2021-03-03_14-09-45-730.csv";
test(gate, csv);

%% CNOT system
gate = "cnot_system";
csv = "cnot_system-identity-N-2_2021-02-24_18-14-18-736.csv";
test(gate, csv);

%% Bitflip
gate = "bitflip";
csv = "bitflip-identity-N-2_2021-03-03_14-12-06-998.csv";
test(gate, csv);

function test(gate, csv)
    N = 2;
    iter = 20;
    dir = "/home/anian/polybox/Masterthesis/Simulation/Data/";
    interaction = "identity";
    verbose = 0;
    true_data = load(dir + csv);

    store_dat = simulation(N, iter, "gate", gate, "verbose", verbose);
    simulated_data = load(store_dat + ".csv");

    diff = true_data - simulated_data
    err = max(abs(diff),[],'all');
    assert( err <= 1e-3);
end
