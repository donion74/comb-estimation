function [Cder,h] = createCder(Ctheta, dimensions, theta_value, h, ...
            dpur, verbose)
    %% creates the expression Cder which appears in the primal problem
    %
    % input
    % ---------------------------------------------------------------------
    % Ctheta:       ensemble decomposition, shape [dim, comp] where dim is
    %               the dimension of the Choi operator of the comb and comp
    %               is the number of components in the ensemble
    %               decomposition.
    % dimensions:   dimensions of the Hilbert spaces H_1, ..., H_{2N}.
    % theta_value:  numerical value of theta.
    % h:            minimising Hermitian h from the dual problem. If not
    %               provided, the function solves the dual problem and
    %               finds h.
    % dpur:         dimension of the purifying system.
    % verbose:      takes values 0,1,2,3.
    %
    % output
    % ---------------------------------------------------------------------
    % Cder:         see description of function
    % h:            minimising Hermitian h.
    
    % variables should be of the form [tau, g, t, theta]
    arguments
        Ctheta = -1000;
        dimensions = 2 * ones(2*2,1);
        theta_value = 2*pi/20;
        h = -1000;
        dpur = 1;
        verbose = 2;
    end
    
    if Ctheta == -1000
        disp("no Ctheta provided")
    end
     
    if h == -1000
        % if no h is given
        disp("createCder: find h via dual problem")
        [QFI_dual,h] = minimalQFI(Ctheta, theta_value, dimensions, dpur);
    end
    dimC = size(Ctheta);
    sizeC = dimC(1);
    N = length(dimensions) / 2;
    components = dimC(2);

    Cdot = diff(Ctheta);
    Cdot = double(subs(Cdot, theta_value));
    Ctheta = double(subs(Ctheta, theta_value));
    
    Cdottilde = zeros(sizeC,components);
    Cder = zeros(sizeC,sizeC);
    % create Ndottilde
    for i=1:components
        Cdottilde(:,i) = Cdot(:,i) - 1j*Ctheta * h(:,i);
        Cder = Cder + Cdottilde(:,i) * Cdottilde(:,i)';
    end
    
    Cder = PartialTrace(Cder, 2*N, dimensions);
    Cder = transpose(Cder);
end
