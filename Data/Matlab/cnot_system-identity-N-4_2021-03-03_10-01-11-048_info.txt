%N: 4, iterations: 100 
                  gate: [1x1 string]
                   var: [1x4 sym]
                 reset: [1x1 logical]
               verbose: 3
             plot_data: 1
           save_folder: [1x1 string]
              parallel: 0
               workers: 24
           interaction: [1x1 string]
                     g: 1
                 theta: 0.31416
                  dpur: 1
                  save: 1
                primal: 1
         collisionOnly: 0
                  daux: 1
                copies: 1
 
