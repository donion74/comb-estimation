function QFI = markovNoControl(U, N, var, var_values, dpur)
    %% calculates the QFI for the Markovian case without control.
    %
    % input
    % ---------------------------------------------------------------------
    % U:        unitary of the interaction, shape [dim1*dim2, comp], where
    %           comp is the number of components in the ensemble
    %           decomposition.
    % N:        order of quantum N-comb
    % var:      variables, default [tau, g, t, theta]
    % var_values:  numerical values of var.
    % dpur:     dimension of purifying system.
    %
    % output
    % ---------------------------------------------------------------------
    % QFI:      QFI of the scenario.

    shape = size(U);
    Ctheta = sym(zeros(shape(1)*shape(2),shape(3)^N));
    for i=1:shape(3)^N
        % bin = dec2bin(i,N); wrong implementation
        bin = de2bi(i-1,N,shape(3),'right-msb')+1;
        tmp = U(:,:,bin(1));
        for k=2:N
            tmp = tmp * U(:,:,bin(k));
        end
        Ctheta(:,i) = reshape(transpose(tmp),[],1);
    end
    
    for i=1:length(var)-1
        Ctheta = subs(Ctheta,var(i),var_values(i));
    end
    
    theta_value = var_values(end);
    dimensions = [shape(1),shape(2)];
    QFI = minimalQFI(Ctheta, theta_value, dimensions, dpur);
    
end

function binary = dec2bin(dec,bits)
    dec = dec - 1;
    binary = zeros(bits,1);
    for i=1:bits
        binary(i) = mod(dec,2);
        dec = dec - binary(i);
        dec = dec / 2;
    end
    binary = binary + 1;
end