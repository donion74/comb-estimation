# Variational quantum circuit
 
This directory contains the Python code for the variational quantum circuit.
Some examples can be found in `checkerboard_simulation.py` and `testComb.py`.

## Files

* `Comb.py` contains the class used for combs and the methods for calculating
  the QFI of the circuit and its gradient.
* `PartialSwap.py` contains the code for the two-qubit interaction swap.
* `checkerboard.py` creates a checkerboard ansatz.
* `simulation.py` starts the optimisation and saves the data and
  plots.

## Examples

* simulation

```python
from simulation import Simulation
from checkerboard import checkerboard
import numpy as np
# adjustable parameters
N = 2
total_times = 2
optimisation_steps = 2
gate = "swap"

# map gate to Python file name
Uint = Simulation.Uint[gate] 
theta = np.pi/10
g = 0.0
dir = "../Data/Python/"
interaction = "identity"
comb_check = checkerboard(N, Uint, theta, g)

t_tot = np.linspace(0,21,total_times)
reset = False

sim = Simulation(dir, gate, interaction, comb_check, t_tot, optimisation_steps, reset)
sim.info()
sim.calculate()
```

* setup

```python
import numpy as np
from checkerboard import checkerboard

N = 2
g = 1
theta = np.pi / 10
comb = checkerboard(N, theta, g)
n_params = comb.dof
var = np.random.random(n_params)
```

* calculate measurement probabilities
```python
probs = comb.prob(var)
```

* calculate QFI with given set of parameters
```python
QFI = comb.qfi_circuit(var)
```

* draw circuit
```python
comb.draw_circuit(var)
```

* optimise parameters for single time step
```python
var_opt = comb.optimize(var, iterations=10, eta=1.0)
```
