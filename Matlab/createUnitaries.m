function [U, U_controlfree, U_Markov, U_Markov_controlfree] ...
        = createUnitaries(tau, g, t, theta, gate, copies)
    %% creates the unitary of the system-environment interaction depending 
    %% on the setup (non-Markovian, Markovian, with control, without control)
    %
    % input
    % ---------------------------------------------------------------------
    % tau:      interaction time
    % g:        interaction strength
    % t:        evolution time
    % theta:    evolution frequency
    % gate:     generator of the interaction, available are 'swap',
    %           'cnot_system', 'cnot_environment'
    %
    % output
    % ---------------------------------------------------------------------
    % U:        unitary for non-Markovian with control
    % U_controlfree: unitary for non-Markovian without control
    % U_Markov: unitary for Markovian with control
    % U_Markov_controlfree: unitary for Markovian without control
    
    if gate == "bitflip" || gate == "multipleBitflip"
        initial_state = "maximal_mixed";
    else
        initial_state = "zero";
    end
    
    createU = str2func(gate);
    U_controlfree = createU(tau,g,t,theta, copies);
    sizeU = size(U_controlfree);
    dimS = sqrt(sizeU(1));
    dimE = dimS;
    U = controlfreeToControl(U_controlfree);
    
    if initial_state == "zero"
        U_Markov = sym(zeros(dimS*dimS,dimE));
        for j=1:dimE
            y1 = dimS*(j-1);
            U_Markov(:,j) = ...
             reshape((U_controlfree(1:dimS,y1+1:y1+dimS)), 1, []);
        end
        
        U_Markov_controlfree = sym(zeros(dimS,dimS,dimE));
        for i=1:dimE
            x1 = dimS*(i-1);
            U_Markov_controlfree(:,:,i) = ...
                U_controlfree(x1+1:x1+dimS, 1:dimS);
        end
    elseif initial_state == "maximal_mixed"
        % general approach
%         U_Markov = sym(zeros(dimS*dimS,dimE*dimE));
%         for i=1:dimE
%             for j=1:dimE
%                 x1 = dimS*(i-1);
%                 y1 = dimS*(j-1);
%                 U_Markov(:,dimE*(i-1)+j) = ...
%                   reshape((U_controlfree(x1+1:x1+dimS,y1+1:y1+dimS)), 1, []);
%             end
%         end
%         U_Markov = U_Markov / sqrt(dimE);
        
%         % this ensures minimal number of components
        U_Markov = sym(zeros(dimS*dimS,dimE));
        for j=1:dimE
            y1 = dimS*(j-1);
            U_Markov(:,j) = ...
             reshape((U_controlfree(1:dimS,y1+1:y1+dimS)), 1, []);
        end
        
        U_Markov_controlfree = sym(zeros(dimS,dimS,dimE*dimE));
        for i=1:dimE
            for j=1:dimE
                y1 = dimS*(i-1);
                x1 = dimS*(j-1);
                U_Markov_controlfree(:,:,dimE*(i-1)+j) ...
                    = U_controlfree(x1+1:x1+dimS, y1+1:y1+dimS);
            end
        end
        U_Markov_controlfree = U_Markov_controlfree / sqrt(dimE);
    end
    
end
